import 'package:diseno1/src/pages/basico_page.dart';
import 'package:diseno1/src/pages/botones_page.dart';
import 'package:diseno1/src/pages/scroll_page.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    // SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle.light.copyWith(
    //   statusBarColor: Colors.black
    // ) );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'botones',
      routes: {
        'basico' : (context) => BasicoPage(),
        'scroll' : (context) => ScrollPage(),
        'botones' : (context) => BotonesPage(),
      },
    );
  }
}