import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {

  final estiloTitulo = TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold);
  final estiloSubtitulo = TextStyle(fontSize: 18.0,color:Colors.grey);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SingleChildScrollView(
        child:Column(
          children: <Widget>[

            _imagen(),
            _titulo(),
            _acciones(),
            _texto(),
            _texto(),
            _texto(),
            _texto(),
            _texto(),
            _texto(),
            _texto(),
            

          ],
        )
      )
    );
  }

  Widget _imagen(){
    return Image(
              image: NetworkImage('https://ichef.bbci.co.uk/wwfeatures/live/976_549/images/live/p0/7w/b9/p07wb9xk.jpg'),
            );
  }

  Widget _titulo(){
    return Container(
              padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Río tipo Suiza', style: estiloTitulo,),
                        SizedBox(height: 7.0),
                        Text('Un río bastante grande', style:estiloSubtitulo),
                      ],
                    ),
                  ),
                  Icon(Icons.star, color:Colors.red, size: 30.0,),
                  Text('41',style: estiloSubtitulo,)
                ],
              ),
            );
  }

  Widget _acciones(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _crearaccion(Icons.call,'Call'),
        _crearaccion(Icons.near_me,'Route'),
        _crearaccion(Icons.share,'Share')
      ],
    );
  }

  Widget _crearaccion(IconData icon, String texto){

    return Column(
      children: <Widget>[
        Icon(icon,color:Colors.blue, size:40.0),
        SizedBox(height: 10.0,),
        Text( texto, style:TextStyle(fontSize: 15.0,color:Colors.blue) )
      ],
    );

  }

  Widget _texto(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal:30.0,vertical:20.0),
      child: Text(
        'Ad ullamco aliqua culpa eu consectetur qui duis. Deserunt ut irure excepteur excepteur. Culpa consectetur enim dolor excepteur velit anim pariatur pariatur dolor. Adipisicing in consequat aliquip do.',
        textAlign: TextAlign.justify,
        style: TextStyle( fontSize: 15.0)
      ),
    );
  }

}
