import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children:<Widget>[
          _pagina1(),
          _pagina2()
        ]
      ),
    );
  }

  Widget _pagina1(){

    return Stack(
      children: <Widget>[
        _colorFondo(),
        _imagenFondo(),
        _textos(),
        
      ],
    );

  }

  Widget _colorFondo(){

    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1),
    );
    
  }

  Widget _imagenFondo(){

    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image:AssetImage('assets/img/portada.png'),
        fit:BoxFit.cover
      ),
    );

  }

  Widget _textos(){

    final estiloTexto = TextStyle(color:Colors.white, fontSize:50.0);

    return SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text('11°', style: estiloTexto,),
                Text('Miércoles', style: estiloTexto,),
                Expanded(child: Container(),),
                Icon(Icons.keyboard_arrow_down,size: 100.0,color:Colors.white)
              ],
      ),
    );

  }

  Widget _pagina2(){

    return Stack(
      children: <Widget>[
        _colorFondo(),
        _botonBienvenida()
        
      ],
    );

  }

  Widget _botonBienvenida(){

    return Center(
            child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
            ),
            padding: EdgeInsets.symmetric(horizontal:50.0,vertical: 10.0),
            onPressed: (){}, 
            child: Text('Bienvenido', style: TextStyle(fontSize: 20.0, color: Colors.white),),
            color: Color.fromRGBO(1,152,255, 1.0),
            elevation: 5.0,
          ),
    );

  }
}